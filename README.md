**COUCOU**

Welcome to the wonderfull world of GIT. GIT is a version-control system for tracking changes in source code during software development. Authorship of all contributions to a project is transparent and all development stages are memorized and can be restored. GIT is a tool to allow collaborative code development in multiple parallely existant versions of the same code-project. GIT enables developers to comment their changes to make them understandable and discussable.

---

# Introduction and Tutorial

This platform for GIT projects is intended to promote a fluid exchange, deployment and development of software among us in CELIA and between us and our collaborators. This tutorial focuses on the establishment of a clean working environment, the performance of first tests and the perspective to practical workflow examples.
*This platform has the IP 192.168.2.202 and should be in reach if you're either connected to a network plug inside CELIA or if you connect to the VPN of CELIA. The platform has internal links (e.g. downloads of releases) that are leading to adresses starting with `http://umr5107.lab/` - this may be not resolved. Then just replace `http://umr5107.lab/` with `192.168.2.202/`.*

---

## GIT Environment

[GIT](https://git-scm.com/downloads) should be installed on your system. In windows you may check if `git bash` is installed, a clear indicator. On Linux you may type `which git` and see if there is a source directory existing. If no GIT can be tracked on your computer, contact the [SRI](mailto:admins@celia.u-bordeaux.fr).
First we clone this project onto your computer. A git project is often referred to as git repository. Open git bash or a terminal and navigate to a clean folder, e.g. `C:\Users\USER\Documents\git` or `user@computer:~/git/hgg$`. Then *clone* the git repository (replace `USER` with your user name on this platform)
```
git clone http://USER@192.168.2.202/celia/hgg.git

```

---

## Python Environment

We would like to play with some simple scripts during the tutorial. Python is ideal for tests. We recommend to set up a clean python environment with [Anaconda](https://www.anaconda.com/distribution/), best would be version 3. We motivate to set up a test project using python version 3.5. as described hereinafter.

### Windows
Open Anaconda navigator, navigate to *Environments* and *Create* an environment with name **hgg** based on python3.5 - once loaded, select the environment and click on the play button behind it to launch the anaconda prompt. You can also just launch the Anaconda Prompt once the environment was created. Then install some packages:
```
(base) C:\Users\USER\Documents\git\hgg> activate hgg
(hgg) C:\Users\USER\Documents\git\hgg> conda install -n hgg numpy matplotlib scipy pandas
```

### Linux
```
(base) user@computer:~/git/hgg$ conda create -n hgg python=3.5 anaconda
(base) user@computer:~/git/hgg$ conda activate hgg
(hgg) user@computer:~/git/hgg$ conda install -n hgg numpy matplotlib scipy pandas
```

## Basic Commands

After your clone command, your local repository `hgg` is made up of three "trees" managed by git. The first is your **workspace** which actually contains your files. The second is an **index** which plays a role for tracking for your files and finally **HEAD** which points to the last validation that you made.

If you navigate to your **workspace** via the file explorer of your computer, you will find only hidden files in it. With a terminal in Linux (or the Anaconda Prompt in Windows), you may ask git for it's status using `git status`
```
(hgg) user@computer:~/git/hgg$ git status
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
```
This message indicates that you did not do any changes to the project on your side. You directly see the reference `origin/master` indicating the remote server-sided root for your working directory. You also can ask for the status of the project on the server side with `git fetch --dry-run --verbose`
```
git fetch --dry-run --verbose
From http://192.168.2.202/celia/hgg
 = [up to date]      master     -> origin/master
```
This message indicates that there are no changes on the remote side. Also, note that we seem to be in an environment called `master`. This is the version of the project we are working on - a so called branch. The `master` branch is supposed to be the state-of-the art running version of the code. Any development should be avoided on the `master` branch. We can list all branches by `git branch -v`. Output of this command is each active branch with it's status. A status is usually the id-number of the last change made and the text message dropped by the developer responsible for the change. You may create your own branch in the project: please use your username instead of `USER` and switch to your branch by
```
(hgg) user@computer:~/git/hgg$ git checkout -b USER
Switched to a new branch 'USER'
```
and still, the status will be clean
```
(hgg) user@computer:~/git/hgg$ git status
On branch USER
nothing to commit, working tree clean
```
The branch exists now only locally, not yet remotely. In order to transport local changes to the remote server, we can use the `git push` command. We can now choose a root name for our remote branch, in this case we only have `origin` available, and the remote name for our local branch, in this case we maintain the same name
```
(hgg) user@computer:~/git/hgg$ git push --set-upstream origin USER
Total 0 (delta 0), reused 0 (delta 0)
remote:
remote: To create a merge request for USER, visit:
remote:   http://umr5107.lab/celia/hgg/merge_requests/new?merge_request%5Bsource_branch%5D=USER
remote:
To http://192.168.2.202/celia/hgg.git
 * [new branch]      USER -> USER
Branch 'USER' set up to track remote branch 'USER' from 'origin'.
```
After this action, there are multiple branches active. We can change between branches with the command `git checkout BRANCHNAME`.
We will now introduce some changes locally: Let's create an cross-platform environment file with Anaconda
```
(hgg) user@computer:~/git/hgg$ conda update conda
(hgg) user@computer:~/git/hgg$ conda env export --from-history > hgg-USERSTATEMENT.yml
```
as we may be many, it may be of some interest to figgure out a `USERSTATEMENT` that is unique, to fully benefit from the purpose of this test. Asking git for the status then unreveals our new file
```
(hgg) user@computer:~/git/hgg$ git status
On branch USER
Your branch is up to date with 'origin/USER'.
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        hgg-USERSTATEMENT.yml

no changes added to commit (use "git add" and/or "git commit -a")
```
This message may seem schizophrenic as git constates us that our branch is up-to-date but the same time some files are untracked. Here we come back to the separation into **workspace**, **HEAD** and **index**. The workspace was changed, as we added a file, but we did not yet introduce it correctly to the project. Git even gives us the hint how to introduce the files: first `git add` them in order to bring them into the **index** and then `git commit` them in order to present them as latest version of the project on your side, roughly the **HEAD**. If ever we did some changes we better should not have done, we can delete all of them by getting the last commited version of the concerned file from **HEAD** via `git checkout --FILENAME`. We now continue our work with
```
(hgg) user@computer:~/git/hgg$ git add hgg-caffee.yml

(hgg) user@computer:~/git/hgg$ git commit -am"MESSAGE"
[USER 5ab092b] MESSAGE
 1 file changed, 16 insertions(+), 0 deletions(-)
 create mode 100644 hgg-USERSTATEMENT.yml
```
where we introduce a qualified message to make people understand what we are about to do, e.g. "MESSAGE" => "Include Conda Environment into project to allow new users a quick set-up of their working environment.". Now we again ask git for the status
```
(hgg) user@computer:~/git/hgg$ git status
On branch USER
Your branch is ahead of 'origin/USER' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```
Git again tells us what to do next - but note before that it confirms us that we added all files we could add and made clear all changes we could make clear with the message *working tree clean*. Now we indeed can `git push origin` or simply `git push` to publish our local changes to the remote server
```
(hgg) user@computer:~/git/hgg$ git push
Enumerating objects: 9, done.
Counting objects: 100% (9/9), done.
Delta compression using up to 12 threads
Compressing objects: 100% (7/7), done.
Writing objects: 100% (7/7), 1.96 KiB | 1.96 MiB/s, done.
Total 7 (delta 2), reused 0 (delta 0)
remote:
remote: To create a merge request for USER, visit:
remote:   http://umr5107.lab/celia/hgg/merge_requests/new?merge_request%5Bsource_branch%5D=USER
remote:
To http://192.168.2.202/celia/hgg.git
   239a33a..4511a58  USER -> USER
```
Branches are usually used to promote changes to the code that are done to meet a certain goal. Due to the branching, different teams can work on their version independently. In order to re-integrate the content of the branch to the `master` branch, we `git merge` the branch back to `master`. Therefore we need to be on `master` and call the branch via
```
(hgg) user@computer:~/git/hgg$ git checkout master
(hgg) user@computer:~/git/hgg$ git merge USER
Updating 2cb8711..2bd9d81
Fast-forward
 hgg-caffee.yml | 16 ++++++++++++
 1 files changed, 16 insertions(+), 0 deletions(-)
 create mode 100644 hgg-USERSTATEMENT.yml

```
This gives a nice summary of the work in the branch, that enerts now `master`. To widen the introduction of branches, please note that with [GIT-SCM](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging) there are more examples available.
Now, we can either keep the branch alive for later work or kill it via `git branch -d USER`. By the way, if you want to push the content of all branches locally stored in **HEAD**, then perform a `git push --all`.
In order to receive changes made by other users on the project, we can pull data from the server via `git pull`.
A tutorial in French language is available with [this nice website](http://rogerdudler.github.io/git-guide/index.fr.html).

---

# Collaboration using GIT

It is now clear how to manage a git repository as only contributor. Now we will introduce the possibilities if the team size increases or if external collaborations are introduced to the project. The following section will treat examples of attitude and workflow in a larger development environment.

---

## Migrate Projects

If you work in a git environment and if you want to transport the project to another git environment, [you may use git-bundle](https://linux.die.net/man/1/git-bundle). Bundles can be used for two scenarios: firstly, to move the project from a server to a client for local work, and secondly, they can be used to install a cloned repository on another server.

#### Create a Bundle
We are working from a system that has access to the git repository of our test repository `hgg` that lies in `~/git/hgg`. In order to create a bundle of all current branches and all the history, do
```
(hgg) user@computer:~/git/hgg$ git bundle create hgg.bundle --all
(hgg) user@computer:~/git/hgg$ git tag -f hgg master
```
The last line will establish an ancor - usefull if you wanted to create an update that only contained changes done after you exported the master bundle. To create such update bundle, you would do
```
(pySTART) user@computer:~/git/hgg$ git bundle create hggXupdate.bundle hgg..master --all
(pySTART) user@computer:~/git/hgg$ git tag -f hggXupdate master
```
tagging the update.

#### Use the Bundle for Migration onto a Computer
On a computer that has access to the destination, you may place the `hgg.bundle` into any `/path/to/bundle/`. In the most simple case, we want to establish a local version of the hgg repository. This local clone may have `any-name`. Then go to your GIT root and perform a clone operation to create the repository from the `hgg.bundle` file
```
(hgg) user@destination:~$ cd ~/git
(hgg) user@destination:~/git$ git clone /path/to/bundle/hgg.bundle any-name
```
Herewith we created a new repository `any-name` into the folder `/git` and we're ready to start work with new commits.

#### Use the Bundle for Migration onto a Server
Otherwise, if you deal with a remote server, we need to  
1) create an empty repository, e.g. with even the same name `hgg` on the git of the remote server
2) `git clone` the empty repository into a folder on your computer
As an example, we will create a clone of the `hgg` repository owned by CELIA onto your account on gitLAB. You can create a new empty repository with `any-name` on your account, then clone it onto your machine
```
(hgg) user@accessdestination:~/git$ git clone http://USER@192.168.2.202/USER/any-name.git
```
Then, we're ready to add-in the bundle information via
```
(hgg) user@accessdestination:~/git/any-name$ git remote add -f hgg /path/to/hgg.bundle
(hgg) user@accessdestination:~/git/any-name$ git merge hgg/master --allow-unrelated-histories
(hgg) user@accessdestination:~/git/any-name$ git push
```
Herewith we created a new repository `any-name` onto a server and linked it into the folder `/git`. The repository contains all information of the ancient repository `hgg` - and we're ready to start work with new commits.

#### Update Scenario
For update cases, where you already dropped a bundle from your *computer A* on another machine, identical to or connected with *computer B*, but you do further changes on your *computer A*. First create the update bundle as described above and then drop it into the GIT root folder of *computer B*
```
(hgg) user@computerB:~/git/any-name$ git bundle verify ../hggXupdate.bundle
(hgg) user@computerB:~/git/any-name$ git fetch ../hggXupdate.bundle
```
This will list what commits you must have in order to extract from the bundle and will error out if you do not have them. 

#### Migrate without Bundle
In cases where 
- you have an existing git repository sitting on your filesystem, and
- the new destination `https://destination.org` can be reached from your machine,
it is sufficient to create a new bond between the new `remote origin` and the local `origin`. After defining a new `remote origin`, here with name `newborn`, you can push the project upstream.
```
(hgg) user@computer:~/git/hgg$ git remote add newborn https://destination.org
(hgg) user@computer:~/git/hgg$ git push newborn --all
(hgg) user@computer:~/git/hgg$ git push newborn --tags
```
and `git remote -v` lists you all remote connections.

---

## FAQ

1. Coffee?
Yes.
